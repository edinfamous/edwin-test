<?php

namespace Drupal\edwin_module\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\Core\Plugin\Context\ContextProviderInterface;
use GuzzleHttp\ClientInterface;

/**
 * Provides automated tests for the edwin_module module.
 */
class HomeControllerTest extends WebTestBase {

  /**
   * Drupal\Core\Plugin\Context\ContextProviderInterface definition.
   *
   * @var \Drupal\Core\Plugin\Context\ContextProviderInterface
   */
  protected $userCurrentUserContext;

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "edwin_module HomeController's controller functionality",
      'description' => 'Test Unit for module edwin_module and controller HomeController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests edwin_module functionality.
   */
  public function testHomeController() {
    // Check that the basic functions of module edwin_module.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
