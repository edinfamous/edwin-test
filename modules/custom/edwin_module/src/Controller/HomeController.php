<?php

namespace Drupal\edwin_module\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class HomeController.
 */
class HomeController extends ControllerBase
{

  /**
   * @var
   */
  protected $currentUser;

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  private $database;

  /**
   * @var
   */
  private $edwinService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    $instance = parent::create($container);
    $instance->currentUser = $container->get('current_user');
    $instance->httpClient = $container->get('http_client');
    $instance->database = $container->get('database');
    $instance->edwinService = $container->get('edwin_module.edwin_services');
    return $instance;
  }

  /**
   * Welcome.
   *
   * @return string
   *   Return Hello string.
   */
  public function welcome()
  {
    if ($this->currentUser()->isAnonymous()) {
      $build = [
        '#theme' => 'edwin_module',
        '#pokemons' => [],
        '#next' => '',
        '#previous' => '',
        '#attached' => [
          'library' => [
            'edwin_module/edwin-module',
          ],
        ],
      ];
      return $build;
    } else {
      $build = [
        '#type' => 'markup',
        '#theme' => 'edwin_module',
        '#pokemons' => [],
        '#next' => '',
        '#previous' => '',
        '#attached' => [
          'library' => [
            'edwin_module/edwin-module',
          ],
        ],
      ];
      $request = $this->httpClient->request('GET', 'https://pokeapi.co/api/v2/pokemon/');

      if ($request->getStatusCode() != 200) {
        return $build;
      }

      $pokemons = json_decode($request->getBody()->getContents());
      $build['#next'] = $pokemons->next;
      $build['#previous'] = $pokemons->previous;

      foreach ($pokemons->results as $key => $pokemon) {
        $build['#pokemons'][] = [
          'name' => $pokemon->name,
          'url' => $pokemon->url,
          'class' => $this->edwinService->findFavorite($pokemon->name) ? 'unfavorite' : 'favorite'
        ];
      }
      return $build;
    }
  }


  /**
   * @param $name
   * @return JsonResponse
   */
  public function addFavorite($name)
  {
    $user = $this->currentUser()->id();
    $information = $this->edwinService->relation($user, $name);

    if (!empty($information)) {
      return new JsonResponse([
        'data' => t('you already have it as a favorite '),
        'method' => 'GET',
      ]);
    } else {
      $count = $this->edwinService->countData($user);
      if ($count < 10) {
        $data = $this->edwinService->insertFavorite($user, $name);
        return new JsonResponse([
          'data' => t('Pokemon added correctly '),
          'method' => 'GET',
        ]);
      } else {
        return new JsonResponse([
          'data' => t('You exceeded the limit favorite, you need to remove some for adding more'),
          'method' => 'GET',
        ]);
      }
    }
  }

  /**
   * @param $name
   * @return int
   */
  public function removeFavorite($name)
  {
    $query = $this->database->delete('favorite_pokemon')
      ->condition('uid', $this->currentUser()->id())
      ->condition('name_pokemon', $name)
      ->execute();
    return new JsonResponse([
      'data' => $query,
      'method' => 'DELETE',
    ]);
  }

  /**
   * @param $name
   * @return JsonResponse
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function compareFavorite($name){
    $request = $this->httpClient->request('GET', 'https://pokeapi.co/api/v2/pokemon/' . $name . '/');
    $pokemons = json_decode($request->getBody()->getContents());
    return new JsonResponse([
      'data' => $pokemons,
      'method' => 'GET',
    ]);
  }
}
