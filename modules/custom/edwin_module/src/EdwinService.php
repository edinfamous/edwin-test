<?php

namespace Drupal\edwin_module;
use Drupal\Core\Session\AccountProxyInterface;
use GuzzleHttp\ClientInterface;
use Drupal\Core\Database\Driver\mysql\Connection;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class EdwinService.
 */
class EdwinService {

  /**
   * Drupal\Core\Session\AccountProxyInterface definition.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Drupal\Core\Database\Driver\mysql\Connection definition.
   *
   * @var \Drupal\Core\Database\Driver\mysql\Connection
   */
  protected $database;

  /**
   * Constructs a new EdwinService object.
   * @param AccountProxyInterface $current_user
   * @param ClientInterface $http_client
   * @param Connection $database
   */
  public function __construct(AccountProxyInterface $current_user, ClientInterface $http_client, Connection $database) {
    $this->currentUser = $current_user;
    $this->httpClient = $http_client;
    $this->database = $database;
  }

  /**
   * @param $uuid
   * @param $name
   * @return mixed
   */
  public function relation($uuid, $name)
  {
    $query = $this->database->select('favorite_pokemon', 'f')
      ->fields('f')
      ->condition('f.uid', $uuid, '=')
      ->condition('f.name_pokemon', $name, '=')
      ->execute()->fetchAll(\PDO::FETCH_ASSOC);
    return $query;
  }

  /**
   * @param $uuid
   * @param $name
   * @return mixed
   */
  public function insertFavorite($uuid, $name)
  {
    $query = $this->database->insert('favorite_pokemon')->fields([
      'uid',
      'name_pokemon',
    ])->values([
      'uid' => $uuid,
      'name_pokemon' => $name,
    ])->execute();
    return $query;
  }

  /**
   * @param $uuid
   * @return int
   */
  public function countData($uuid)
  {
    $query = $this->database->select('favorite_pokemon', 'f')
      ->fields('f')
      ->condition('f.uid', $uuid)
      ->execute()->fetchAll(\PDO::FETCH_ASSOC);
    return count($query);
  }

  /**
   * @param $user
   * @return mixed
   */
  public function listMyFavorites($user)
  {
    $query = $this->database->select('favorite_pokemon', 'f')
      ->fields('f')
      ->condition('f.uid', $user)
      ->execute()->fetchAll(\PDO::FETCH_ASSOC);
    return $query;
  }

  /**
   * @param $name
   * @return bool
   */
  public function findFavorite($name)
  {
    $favorites = $this->listMyFavorites($this->currentUser->id());
    $status = false;
    foreach ($favorites as $favorite) {
      if ($name == $favorite['name_pokemon']) {
        $status = true;
      }
    }
    return $status;
  }

}
