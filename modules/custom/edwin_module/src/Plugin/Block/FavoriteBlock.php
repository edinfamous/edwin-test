<?php

namespace Drupal\edwin_module\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'FavoriteBlock' block.
 *
 * @Block(
 *  id = "favorite_block",
 *  admin_label = @Translation("Favorite block"),
 * )
 */
class FavoriteBlock extends BlockBase implements ContainerFactoryPluginInterface
{
  /**
   * @var
   */
  protected $edwinService;

  /**
   * @var
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->edwinService = $container->get('edwin_module.edwin_services');
    $instance->currentUser = $container->get('current_user');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $build = [];
    $build['#theme'] = 'favorite_block';
    $build['#attached'] = [
      'library' => [
        'edwin_module/edwin-module',
      ],
    ];
    $pokemons = $this->edwinService->listMyFavorites($this->currentUser->id());
    $build['#pokemons'] = $pokemons;
    return $build;
  }

}
