(function ($, Drupal) {
  Drupal.behaviors.edwin_behavior = {
    attach: function (context, settings) {
      /**
       *
       */
      $('.add-favorite.favorite', context).once('edwin_module').on('click', function () {
        // some code
        var dataUrl = $(this).attr('data-url');
        var dataName = $(this).attr('data-name');
        var pokemon = $(this);
        $.ajax({
          url: Drupal.url('add-favorite/' + dataName),
          type: "GET",
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (response) {
            $('#message').modal();
            $('#message .modal-body p').text(response.data);
            pokemon.remove('favorite');
            pokemon.remove('unfavorite');
            Drupal.attachBehaviors();
          }
        });
      });

      /**
       *
       */
      $('.add-favorite.unfavorite', context).once('edwin_module').on('click', function () {
        // some code
        var dataUrl = $(this).attr('data-url');
        var dataName = $(this).attr('data-name');
        var pokemon = $(this);
        console.log('Hello');
        $.ajax({
          url: Drupal.url('remove-favorite/' + dataName),
          type: "DELETE",
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function () {
            $('#message').modal();
            $('#message .modal-body p').text('Delete correctly pokemon ' + dataName);
            pokemon.remove('favorite');
            pokemon.remove('unfavorite');
            Drupal.attachBehaviors();
          }
        });
      });

      /**
       *
       */
      $('.compare', context).once('edwin_module').on('click', function () {
        // some code
        $('#loader').modal();
        var dataName = $(this).attr('data-name');
        var pokemonSite = '';
        var pokemon = localStorage.getItem('pokemon');
        var pokemon1 = localStorage.getItem('pokemon1');

        if (pokemon.length == undefined || pokemon.length == 0) {
          localStorage.setItem('pokemon', dataName);
          pokemonSite = '.right';
          compare(dataName, pokemonSite);
        } else if (pokemon1.length == undefined || pokemon1.length == 0) {
          pokemonSite = '.left';
          compare(dataName, pokemonSite);
          localStorage.setItem('pokemon1', dataName);
        } else {
          localStorage.setItem('pokemon', '');
          localStorage.setItem('pokemon1', '');
          $('.left').empty();
          $('.right').empty();
          $('#loader').modal('hide');
          pokemonSite = '';
        }
      });

      /**
       *
       * @param dataName
       * @param pokemonSite
       */
      function compare(dataName, pokemonSite) {
        $.ajax({
          url: Drupal.url('compare-favorite/' + dataName),
          type: "GET",
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (response) {
            var html = '<div class="card">' +
              '<div class="card-header">' +
              '<img src="' + response.data.sprites.front_default + '" />' +
              '</div>' +
              '<div class="card-body  card-5-7">' +
              '<h2>' + dataName + '</h2>' +
              '<h3> Experience: ' + response.data.base_experience + '</h3>' +
              '<span> Height: ' + response.data.height + '</span>' +
              '<span> Specie: ' + response.data.species.name + '</span>' +
              '<h3>Habilities</h3>' +
              '<ul class="list-unstyled mb-4">';

            for (var i = 0; i < response.data.abilities.length; i++) {
              html = html + '<li>' + response.data.abilities[i].ability.name + '</li>';
            }
            html = html + '</ul>' +
              '<h3>Stats</h3>' +
              '<ul>';

            for (var i = 0; i < response.data.stats.length; i++) {
              html = html + '<li>' + response.data.stats[i].stat.name + ': ' + response.data.stats[i].base_stat + ', effort:' + response.data.stats[i].effort + '</li>';
            }
            html = html + '</ul>' +
              '</div>' +
              '</div>';
            $(pokemonSite).append(html);
            $('#loader').modal('hide');
            Drupal.attachBehaviors();
          }
        });
      }
    }
  };
})(jQuery, Drupal);
